{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}

module Main where

-- maybe consider starting over with an easier system. look here:
-- https://github.com/HaskellMN/haskell-mn-twitter-bot/blob/master/src/Main.hs
-- https://github.com/AshyIsMe/lambdatwit/blob/master/Main.hs
-- https://www.reddit.com/r/haskell/comments/2hol6t/lambda_twit_lambdatwit_haskell_evaluating_twitter/
-- https://www.reddit.com/r/haskell/comments/21g8g8/functional_reactive_twitter_bots/
-- https://github.com/pvrnav/haskell-twitter-bot/blob/master/Main.hs 

-- or try this if you can ever get it to install 
-- https://hackage.haskell.org/package/hs-twitter
-- http://haskell.forkio.com/twitter
-- could always rewrite myself if necessary. but these conduits are fucking killing me

import           Control.Applicative ((<$>))
import           Control.Exception
import           Control.Lens hiding ((|>))
import           Control.Monad
import           Control.Monad.IO.Class
import           Control.Monad.Reader (ask)
import           Control.Monad.State (get, put)
import           Control.Monad.Trans.Control (MonadBaseControl)
import           Control.Monad.Trans.Resource
import qualified Data.ByteString.Char8 as B8
import qualified Data.Conduit as C
import qualified Data.Conduit.List as CL
import qualified Data.Map.Strict as Map
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import           Data.Typeable (Typeable)
import           Network.HTTP.Conduit
import           System.Environment (getEnv)
import           System.IO (hFlush, readFile, stdout, writeFile)
import qualified Web.Authenticate.OAuth as OA
import           Web.Twitter.Conduit
import           Web.Twitter.Types.Lens

type TweetText = T.Text
type Username  = T.Text

data ReducedStatus = ReducedStatus {
    rdsText     :: TweetText
  , rdsUserName :: Username
    -- type LanguageCode = String
  , rdsLang     :: Maybe LanguageCode
    -- Integer
  , rdsStatusId :: StatusId
  } deriving (Show, Typeable)

authorize :: (MonadBaseControl IO m, MonadResource m)
          => OA.OAuth -- ^ OAuth Consumer key and secret
          -> (String -> m String) -- ^ PIN prompt
          -> Manager
          -> m OA.Credential
authorize oauth getPIN mgr = do
    cred <- OA.getTemporaryCredential oauth mgr
    let url = OA.authorizeUrl oauth cred
    pin <- getPIN url
    OA.getAccessToken oauth
      (OA.insert "oauth_verifier" (B8.pack pin) cred) mgr

getTWInfo :: IO TWInfo
getTWInfo = do
  key <- getEnv "OAUTH_KEY"
  secret <- getEnv "OAUTH_SECRET"
  let tokens = twitterOAuth {
          OA.oauthConsumerKey = B8.pack key
        , OA.oauthConsumerSecret = B8.pack secret
        }
  mgr <- newManager tlsManagerSettings
  cred <- runResourceT $ authorize tokens getPIN mgr
  return $ setCredential tokens cred OA.def
  where
    getPIN url = liftIO $ do
        putStrLn $ "browse URL: " ++ url
        putStr "> what was the PIN twitter provided you with? "
        hFlush stdout
        getLine

printStatus :: Status -> IO ()
printStatus status = TIO.putStrLn texty
  where texty = T.concat [ T.pack . show $ status ^. statusId
                         , ": "
                         , status ^. statusUser . userScreenName
                         , ": "
                         , status ^. statusText
                         ]

toReducedStatus :: Status -> ReducedStatus
toReducedStatus status = rds
  where rds = ReducedStatus txt user lang sid
        txt  = status ^. statusText
        user = status ^. statusUser . userScreenName
        lang = status ^. statusLang
        sid  = status ^. statusId

foldStream :: TWInfo
           -> Int
           -> UserParam
           -> IO ()
foldStream twInfo numTweets user = do
  mgr <- newManager tlsManagerSettings
  runResourceT $ sourceWithMaxId twInfo mgr (userTimeline user)
    C.$= CL.isolate numTweets
    C.$$ CL.mapM_ (\t -> liftIO (print t))

killFollows :: TWInfo -> IO ()
killFollows twInfo = do
  mgr <- newManager tlsManagerSettings
  runResourceT $ call twInfo mgr $ friendshipsDestroy (ScreenNameParam "chorwus")
  return ()
-- on my desktop, the getEnv / withCredentials isn't working for reasons i do not yet understand
-- on this machine with previously stashed credentials, it *seems* to be doing something but does not actually
-- delete any follows at all. so. that was all for nought. it does compile tho.
-- i should maybe try something easier for my first Haskell project. this is what happens when you never write
-- code.

  -- runResourceT $ sourceWithCursor twInfo mgr $ friendsList (ScreenNameParam "argumatronic")
  -- C.$$ CL.mapM_ undefined

  -- ((\t -> void $ call twInfo _ $ friendshipsDestroy (t ^. userName)))
--  call twInfo mgr $ friendshipsDestroy (ScreenNameParam "thimura")


-- killFavs :: TWInfo
--          -> IO ()
-- killFavs twInfo = do
--   mgr <- newManager tlsManagerSettings
--   runResourceT $ sourceWithMaxId twInfo mgr (favoritesList Nothing)
--     -- C.$= CL.isolate 1
--     C.$$ CL.mapM_ (\t -> void $ call twInfo mgr $ favoritesDestroy (t ^. statusId))

saveCredentials :: TWInfo -> IO ()
saveCredentials twinfo = writeFile ".credentials" (show twinfo)

loadCredentials :: IO (Maybe TWInfo)
loadCredentials = do
  tw <- readFile ".credentials"
  return $ Just (read tw)

withCredentials :: (TWInfo -> IO ()) -> IO ()
withCredentials action = do
  twinfo <- loadCredentials `catch` handleMissing
  case twinfo of
    Nothing     -> getTWInfo >>= saveCredentials
    Just twinfo -> action twinfo
  where handleMissing :: IOException -> IO (Maybe TWInfo)
        handleMissing _ = return Nothing

main :: IO ()
main = do
  withCredentials killFollows
  -- withCredentials killFavs
    -- twInfo <- getTWInfo
    -- m <- foldStream twInfo 100 (ScreenNameParam "argumatronic")
    -- killFavs twInfo
